
import java.util.ArrayList;
import java.util.Random;

import junit.framework.TestCase;
import model.data_structures.ListaDoblementeEncadenada;


/**
 * Esta es la clase usada para verificar que los m�todos de la clase ListaDoblementeEncadenada est�n correctamente implementados
 */
public class LinkedListTest extends TestCase
{
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------

    /**
     * Es la clase donde se har�n las pruebas
     */
    private ListaDoblementeEncadenada<Integer> linkedList;

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

    /**
     * Construye un nuevo LinkedList vac�o
     * 
     */
    private void setupEscenario1( )
    {
        linkedList = new ListaDoblementeEncadenada<Integer>( );
    }

    /**
     * Construye un nuevo LinkedList con 3 integers.
     */
    private void setupEscenario2( )
    {
    	linkedList = new ListaDoblementeEncadenada<Integer>( );

        try
        {
            linkedList.add(0);
            linkedList.add(1);
            linkedList.add(2);
            linkedList.add(3);
            linkedList.add(4);
        }
        catch( Exception e )
        {
            fail( "Error en el metodo add" );
        }
    }

    /**
     * Construye un linkedlist con 4 integers
     */
    private void setupEscenario3( )
    {
    	linkedList = new ListaDoblementeEncadenada<Integer>( );
        try
        {

        	linkedList.add(0);
            linkedList.add(1);
            linkedList.add(2);
            linkedList.add(3);
            linkedList.add(4);
        }
        catch( Exception e )
        {
            fail( "Revisa el m�todo add." );
        }

  
    }