package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {

	Integer getSize();
	
	public boolean add( T newT);
	
	public void addAtEnd( T newT);
	
	public void addAtK( int index, T k);
	
	public T getElement(int index);
	
	public T getCurrentElement();
	
	public void delete( T toDelete);
	
	public T deleteAtK(int k);
	
	public T next();
	
	public T previous();



}
