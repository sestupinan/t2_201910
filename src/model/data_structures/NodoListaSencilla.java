package model.data_structures;

/**
 * 
 * @author Lily Duque Chavez
 * Representa un nodo de una lista sencillamente enlazada.
 * @param <E>
 */
public class NodoListaSencilla<E>
{
	
	/**
	 * Elemento almacenado en el nodo.
	 */
	private E elemento;
	/**
	 * Siguiente nodo.
	 */
	private NodoListaSencilla<E> siguiente;
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenar� en el nodo. elemento != null
	 */
	public NodoListaSencilla(E pElemento)
	{
		elemento = pElemento;
		siguiente = null;
	}
	
	/**
	 * M�todo que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(NodoListaSencilla<E> pSiguiente)
	{
		siguiente = pSiguiente;
	}
	
	/**
	 * M�todo que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenar� en el nodo.
	 */
	public void cambiarElemento(E pElemento)
	{
		elemento = pElemento;
	}
	
	
	/**
	 * M�todo que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public NodoListaSencilla<E> darSiguiente()
	{
		return siguiente;
	}

}
