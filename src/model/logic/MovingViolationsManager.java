package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.opencsv.CSVReader;

import api.IMovingViolationsManager;

import model.vo.VOMovingViolations;
import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.MovingViolation;

public class MovingViolationsManager implements IMovingViolationsManager {

	ListaDoblementeEncadenada<MovingViolation> list;
	
	public void loadMovingViolations(String movingViolationsFile) 
	{
		CSVReader reader;
		list = new ListaDoblementeEncadenada<MovingViolation>();
		try 
		{
			
			reader = new CSVReader(new FileReader(movingViolationsFile));
			String [] nextLine = new String[16];
			while ((nextLine = reader.readNext()) != null) 
			{
				nextLine = reader.readNext();
				MovingViolation nuevo = new MovingViolation(Integer.parseInt(nextLine[0]), nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7], Integer.parseInt(nextLine[8]), Integer.parseInt(nextLine[9]), Integer.parseInt(nextLine[10]), nextLine[12], nextLine[13], nextLine[14], nextLine[15]);
				list.add(nuevo);
				
			}
			reader.close();
		} 
		catch (Exception e) 
		{
			e  = new Exception("Hubo un error cargando los archivos");
		}
		
	}

		
	@Override
	public ListaDoblementeEncadenada <MovingViolation> getMovingViolationsByViolationCode (String violationCode)
	{
		ListaDoblementeEncadenada<MovingViolation> buscada = new ListaDoblementeEncadenada<MovingViolation>();
		int i = 0;
		int listSize = list.size();
		if(list != null)
		{
			while(listSize > i && list.get(i) != null)
			{
				if(list.get(i).getViolationCode().compareTo(violationCode) == 0)
				{
					buscada.add(list.get(i));
				}
				i++;
			}
		}
		
		
		return buscada;
	}

	@Override
	public ListaDoblementeEncadenada <MovingViolation> getMovingViolationsByAccident( ) {
		
		ListaDoblementeEncadenada<MovingViolation> buscada = new ListaDoblementeEncadenada<MovingViolation>();
		int i = 0;
		int listSize = list.size();
		if(list != null)
		{
			while(listSize > i && list.get(i) != null)
			{
				if(list.get(i).getAccidentIndicator().equalsIgnoreCase("Yes"))
				{
					buscada.add(list.get(i));		
				}
				i++;
			}
		}


		return buscada;
	}	


}
