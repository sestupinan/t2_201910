package controller;

import api.IMovingViolationsManager;

import model.data_structures.ListaDoblementeEncadenada;
import model.data_structures.MovingViolation;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;

public class Controller {

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IMovingViolationsManager  manager = new MovingViolationsManager();
	
	public static void loadMovingViolations() {
		manager.loadMovingViolations("./data/Moving_Violations_Issued_in_January_2018.csv");
	}
	
	public static ListaDoblementeEncadenada <MovingViolation> getMovingViolationsByViolationCode (String violationCode) {
		return manager.getMovingViolationsByViolationCode(violationCode);
	}
	
	public static ListaDoblementeEncadenada <MovingViolation> getMovingViolationsByAccident(String accidentIndicator) {
		return manager.getMovingViolationsByAccident();
	}
}
